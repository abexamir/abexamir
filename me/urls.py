from django.http import HttpResponse
from django.urls import path
from django.views.generic import DetailView

from . import views
from .views import msg_sender

urlpatterns = [
    path('', msg_sender), ]
