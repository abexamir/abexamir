from django.urls import path
from . import views
from .views import PostDetailView

urlpatterns = [
    path('', views.BlogIndexView.as_view(), name='index'),
    path('<int:pk>/', PostDetailView.as_view(), name='post-detail')
]
