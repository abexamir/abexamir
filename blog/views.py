from django.shortcuts import render

# Create your views here.
from django.views import generic

from blog.models import Post


class BlogIndexView(generic.ListView):
    """
    Generic class-based view for a list of all blogs.
    """
    model = Post
    paginate_by = 5


class PostDetailView(generic.DetailView):
    """
    Generic class-based detail view for a blog.
    """
    model = Post
